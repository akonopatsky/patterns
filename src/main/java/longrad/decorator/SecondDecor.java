package longrad.decorator;

public class SecondDecor implements PartOfSomething{
    private PartOfSomething part;

    public SecondDecor(PartOfSomething part) {
        this.part = part;
    }

    @Override
    public String getInfo() {
        return part.getInfo() + "and add second decor ";
    }
}
