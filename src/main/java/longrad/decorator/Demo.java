package longrad.decorator;

public class Demo {
    public static void main(String[] args) {
        PartOfSomething part0 = new InitialPart();
        PartOfSomething part1 = new FirstDecor(part0);
        PartOfSomething part2 = new SecondDecor(part1);
        System.out.println(part2.getInfo());
    }
}
