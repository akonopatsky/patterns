package longrad.decorator;

public class FirstDecor implements PartOfSomething{
    private PartOfSomething part;

    public FirstDecor(PartOfSomething part) {
        this.part = part;
    }

    @Override
    public String getInfo() {
        return part.getInfo() + "and add first decor ";
    }
}
