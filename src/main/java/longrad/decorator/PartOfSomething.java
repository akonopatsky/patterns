package longrad.decorator;

public interface PartOfSomething {
    String getInfo();
}
