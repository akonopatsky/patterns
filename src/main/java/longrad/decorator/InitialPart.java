package longrad.decorator;

public class InitialPart implements PartOfSomething{
    @Override
    public String getInfo() {
        return "lets begin ";
    }
}
