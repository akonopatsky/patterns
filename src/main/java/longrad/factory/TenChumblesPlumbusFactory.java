package longrad.factory;

import java.awt.*;

public class TenChumblesPlumbusFactory {
    public static Plumbus getPlumbus() {
        return new Plumbus(10);
    }
}
