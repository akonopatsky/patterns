package longrad.factory;

import java.awt.*;

public class Plumbus {
    private int chamblesCount;

    public Plumbus(int chamblesCount) {
        this.chamblesCount = chamblesCount;
    }

    @Override
    public String toString() {
        return chamblesCount + " chambles plumbus";
    }
}
