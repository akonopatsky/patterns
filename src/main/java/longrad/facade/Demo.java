package longrad.facade;

public class Demo {
    public static void main(String[] args) {
        DingleBop dingleBop = new DingleBop();
        Shlami shlami = new Shlami();
        Facade facade = new Facade(dingleBop, shlami);
        facade.doAll();
    }
}
