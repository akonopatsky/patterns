package longrad.facade;

public class Facade {
    private DingleBop dingleBop;
    private Shlami shlami;

    public Facade(DingleBop dingleBop, Shlami shlami) {
        this.dingleBop = dingleBop;
        this.shlami = shlami;
    }

    public void doAll() {
        dingleBop.slimeImpragnation();
        dingleBop.strungOnGrumba();
        dingleBop.rubInLick();
        shlami.rubs();
        shlami.spit();
    }
}
